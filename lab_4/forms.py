from django import forms

class Message_Form(forms.Form):
    error_messages = {
        'message' : {'required': 'Tolong isi input ini!'},
        'email' : {'invalid': 'Isi input dengan email Anda.'},
    }
    attrs = {
        'class': 'form-control'
    }

    name = forms.CharField(label='Nama', required=False, max_length=27, empty_value='Anonymous', widget=forms.TextInput(attrs=attrs))
    email = forms.EmailField(required=False, widget=forms.EmailInput(attrs=attrs), error_messages=error_messages['email'])
    message = forms.CharField(widget=forms.Textarea(attrs=attrs), error_messages=error_messages['message'])
